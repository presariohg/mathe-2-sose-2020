#include <iostream>
#include "C_DGLSolver.h"

CMyVektor f_dgl_system(CMyVektor y, double x) {
    std::vector<double> result = std::vector<double>();

    result.push_back(2 * y[1] - x * y[0]);
    result.push_back(y[0] * y[1] - 2 * x * x * x);

    return CMyVektor(result);
}


double f_dgl_high_order(CMyVektor y, double x) {
    return (2 * x * y[1] * y[2] + 2 * y[0] * y[0] * y[1]);
}


int main() {
//    CMyVektor y0 = CMyVektor({0, 1});
//    double x_start = 0;
//    double x_end = 2;
    CMyVektor y0 = CMyVektor({1, -1, 2});
    double x_start = 1;
    double x_end = 2;

    C_DGLSolver solver = C_DGLSolver(f_dgl_high_order);
//    C_DGLSolver solver = C_DGLSolver(f_dgl_system);

//    CMyVektor y_end = solver.solve_euler(x_start, x_end, 10, y0);
    CMyVektor y_end = solver.solve_heun(x_start, x_end, 100, y0);

    const double Y0_CERTAIN = 0.5;

    std::cout << "End:\n";
    std::cout << "\tx = " << x_end << "\n";
    std::cout << "\ty = " << y_end << "\n";
    std::cout << "\tAbsolute uncertainty = " << std::abs(y_end[0] - Y0_CERTAIN) << "\n";

    return 0;
}
