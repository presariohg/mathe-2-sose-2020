#include <exception>
#ifndef VECTOR_EXCEPTION
#define VECTOR_EXCEPTION

class VectorException {
public:
    struct WrongDimensionException : public std::exception {
        const char * what() const throw() {
            return "Something's wrong with the vector's dimensions";
        }
    };

    struct EmptyValueException : public std::exception {
        const char * what() const throw() {
            return "This vector's value is not set yet.";
        }
    };

    struct InvalidDimensionException : public std::exception {
        const char * what() const throw() {
            return "Vector's dimension is not valid.";
        }
    };

    struct VectorOutOfBoundException : public std::exception {
        const char * what() const throw() {
            return "Index not valid!";
        }
    };
};

#endif