//
// Created by presariohg on 03/05/2020.
//

#ifndef CMYMATRIX_H
#define CMYMATRIX_H

#include <vector>
#include "MatrixException.h"
#include "CMyVektor.h"

class CMyMatrix {
private:

    std::vector <std::vector <double>> matrix;
    bool isMatrixSet = false;
    bool isDimensionSet = false;

    int width;
    int height;


    /**
     * @param matrix the matrix to check compatibility with.
     * @return True if the input matrix is valid. Otherwise false.
     */
    bool verifyDimension(std::vector<std::vector<double>> matrix) {
        // if any row has a different number of columns, return false

        int width = matrix[0].size();
        int height = matrix.size();

        for (int row = 0; row < height; row++) {
            if (matrix[row].size() != width)
                return false;
        }

        // check if the input matrix's dimensions match this matrix's set dimension, if it's already set.
        if (isDimensionSet) {
            if ((width != this->width) || (height != this->height))
                return false;
        }

        return true;
    }

public:
    CMyMatrix() {
        this->isMatrixSet = false;
        this->isDimensionSet = false;
    }


    CMyMatrix(int width, int height) {

        this->matrix.resize(height);

        for (int row = 0; row < height; row++) {
            this->matrix[row].resize(width);
        }

        this->width = width;
        this->height = height;
        this->isMatrixSet = false;
        this->isDimensionSet = true;
    }


    CMyMatrix(std::vector<std::vector<double>> matrix) {

        if (!this->verifyDimension(matrix))
            throw MatrixException::InvalidMatrixException();

        this->matrix = matrix;

        this->width = matrix[0].size();
        this->height = matrix.size();
        this->isMatrixSet = true;
        this->isDimensionSet = true;
    }


    /**
     * Reset dimensions to this matrix. Erase all previous values
     */
    void setDimensions(int width, int height) {
        this->width = width;
        this->height = height;
        this->matrix = std::vector<std::vector<double>>(height);

        for (int row = 0; row < height; row++) {
            this->matrix[row].resize(width);
        }

        this->isMatrixSet = false;
        this->isDimensionSet = true;
    }


    /**
     * @param width The matrix's width. This is a reference (will be replaced).
     * @param height The matrix's height. This is a reference (will be replaced).
     */
    void getDimensions(int &width, int &height) const {
        width = this->width;
        height = this->height;
    }


    /**
     * @param matrix The new matrix to set to this matrix. Note that the new matrix's dimensions must match with
     *               the old one.
     */
    void setMatrix(std::vector<std::vector<double>> matrix) {
        if (!this->verifyDimension(matrix))
            throw MatrixException::InvalidMatrixException();

        this->matrix = matrix;

        this->width = matrix[0].size();
        this->height = matrix.size();
        this->isMatrixSet = true;
        this->isDimensionSet = true;
    }


    /**
     * @return A 2 dimensional vector contains this matrix's value.
     */
    std::vector<std::vector<double>> getMatrix() {
        return this->matrix;
    }


    /**
     * Set a component given its coordinates
     */
    void setComponent(int row, int column, double value) {
        if ((row >= this->height) || (column >= this->width))
            throw MatrixException::MatrixOutOfBoundException();

        if ((row < 0) || (column < 0))
            throw MatrixException::MatrixOutOfBoundException();

        this->matrix[row][column] = value;
    }


    /**
     * Get a component given its coordinates
     * @return The value at (column | row)
     */
    double getComponent(int row, int column) const {
        if ((row >= this->height) || (column >= this->width))
            throw MatrixException::MatrixOutOfBoundException();

        if ((row < 0) || (column < 0))
            throw MatrixException::MatrixOutOfBoundException();

        if (!isMatrixSet)
            throw MatrixException::EmptyMatrixException();

        return this->matrix[row][column];
    }


    /**
     * Invert a 2x2 matrix. Can't do anything else.
     * @return The inverted 2x2 matrix
     */
    CMyMatrix invert() const {
        if ((this->width == 2) &&
            (this->height == 2) &&
            (this->isMatrixSet)) {

            double a = matrix[0][0];
            double b = matrix[0][1];
            double c = matrix[1][0];
            double d = matrix[1][1];

            double param = 1 / (a * d - b * c);

            std::vector<std::vector<double>> invertedMatrix = {{param * d, - param * b},
                                                               {- param * c, param * a}};

            return CMyMatrix(invertedMatrix);
        } else
            throw MatrixException::NonInvertibleMatrixException();
    }


    std::vector<double> operator[] (int row) const {
        if ((row < 0) || (row > this->height))
            throw MatrixException::MatrixOutOfBoundException();

        return this->matrix[row];
    }


    std::vector<double> &operator[] (int row) {
        if ((row < 0) || (row > this->height))
            throw MatrixException::MatrixOutOfBoundException();

        return this->matrix[row];
    }


    friend std::ostream& operator<< (std::ostream & out, const CMyMatrix & matrix);
    friend CMyMatrix operator* (const CMyMatrix& matrix1, const CMyMatrix& matrix2);
};


// Just a bunch of craps to make printing out a matrix prettier
std::ostream& operator<< (std::ostream & out, const CMyMatrix & matrix) {

    if (matrix.height > 1) {
        out << "/ ";
        for (int j = 0; j < matrix.width; j++)
            out << matrix[0][j] << " ";

        out << "\\\n";

        for (int i = 1; i < matrix.height - 1; i++) {
            out << "| ";
            for (int j = 0; j < matrix.width; j++)
                out << matrix[i][j] << " ";

            out << "|\n";
        }

        out << "\\ ";
        for (int j = 0; j < matrix.width; j++)
            out << matrix[matrix.height - 1][j] << " ";

        out << "/\n";
    } else {
        out << "( ";
        for (int j = 0; j < matrix.width; j++)
            out << matrix[0][j] << " ";

        out << ")\n";
    }

    return out;
}


/**
 * Enabling matrices multiplication.
 */
CMyMatrix operator* (const CMyMatrix& matrix1, const CMyMatrix& matrix2) {
    if ((matrix1.width != matrix2.height))
        throw MatrixException::InvalidMatricesDimensionException();

    CMyMatrix result = CMyMatrix();
    result.setDimensions(matrix2.width, matrix1.height);

    for(int row = 0; row < result.height; row++) {
        for (int column = 0; column < result.width; column++) {
            result[row][column] = 0;

            for (int i = 0; i < matrix1.width; i++) {
                result[row][column] += matrix1[row][i] * matrix2[i][column];
            }
        }
    }

    return result;
}


/**
 * Calculate the jacobi matrix of function f at the point x
 * @param x The vector represent the point x in hyperspace.
 * @return The jacobi matrix of f at x, aka Jf(x).
 */
CMyMatrix jacobi(CMyVektor x, CMyVektor (*f)(CMyVektor)) {
    CMyVektor fx = f(x);
    int height = fx.get_dimension();

    std::vector<std::vector<double>> jacobiMatrix;

    jacobiMatrix.resize(height);

    for (int row = 0; row < height; row++) {
        jacobiMatrix[row] = gradient(x, f, row).get_value();
    }

    return CMyMatrix(jacobiMatrix);
}


#endif //CMYMATRIX_H
