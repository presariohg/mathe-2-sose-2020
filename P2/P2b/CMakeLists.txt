cmake_minimum_required(VERSION 3.16)
project(P2b)

set(CMAKE_CXX_STANDARD 14)

include_directories(.)

add_executable(P2b
        CMyVektor.h
        CMyMatrix.h
        main.cpp
        MatrixException.h
        VectorException.h)
