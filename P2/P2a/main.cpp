#include <iostream>
#include <vector>
#include "CMyMatrix.h"


int main() {
    CMyMatrix matrix1 = CMyMatrix({{1, 2},
                                         {0, 1},
                                         {2, 0}});

    CMyMatrix matrix2 = CMyMatrix({{2, 0, 1},
                                         {0, 1, 1}});


    std::cout << matrix1 * matrix2;
//    std::cout << "Hello, World!" << std::endl;
    return 0;
}
