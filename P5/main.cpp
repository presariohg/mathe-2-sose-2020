#include <iostream>
#include "CZufall.h"
#include "CLotto.h"

void aufgabe_a() {
    CZufall::initialisiere(31);
    std::vector<int> foo = CZufall::test(3, 7, 10000);

    for (auto i : foo)
        std::cout << i << "\n";
    std::cout << "\n";

    foo = CZufall::test(3, 7, 10000);

    for (auto i : foo)
        std::cout << i << "\n";
    std::cout << "\n";

    foo = CZufall::test_false(3, 7, 10000);

    for (auto i : foo)
        std::cout << i << "\n";
}

void aufgabe_b() {
    CLotto lotto(-1);

    lotto.set_ticket({1,2,3,7,4,5});

    std::cout << "Your ticket: ";

    for (int number : lotto.get_ticket())
        std::cout << number << " ";
    std::cout << '\n';

    int times = 3;

    for (int i = 0; i < times; i++) {
        std::vector<int> matches = lotto.compare_result(lotto.get_ticket());
        std::vector<int> result = lotto.get_result();
        std::cout << "Round " << i << " result: ";

        for (int number : result)
            std::cout << number << " ";
        std::cout << "\nMatched: ";

        if (matches.empty())
            std::cout << "None. Better try again!\n";
        else
            std::cout << "\nYour ticket matched " << matches.size() << " times! Congratulation!\n\n";
    }
}

int main() {
//    aufgabe_a();
//    aufgabe_b();
    CLotto lotto(-1);
    lotto.monte_carlo_fixed({21, 41, 48, 29, 17, 39}, 3, 100000);
    lotto.monte_carlo_changing(3, 100000);

    return 0;
}
