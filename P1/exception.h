#include <exception>
#ifndef VECTOR_EXCEPTION
#define VECTOR_EXCEPTION

class VectorException {
public:
    struct WrongDimensionException : public std::exception {
        const char * what() const throw() {
            return "Vector's dimension is bigger than and given value's dimension";
        }
    };

    struct EmptyValueException : public std::exception {
        const char * what() const throw() {
            return "This vector's value is not set yet.";
        }
    };

    struct InvalidDimensionException : public std::exception {
        const char * what() const throw() {
            return "Vector's dimension is not valid.";
        }
    };
};

#endif