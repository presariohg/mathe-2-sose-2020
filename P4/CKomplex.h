//
// Created by presariohg on 29/05/2020.
//

//#ifndef P4_CKOMPLEX_H
//#define P4_CKOMPLEX_H
#pragma once
#include <cmath>
#include <iostream>

class CKomplex {
private:
    double real;
    double imaginary;

public:

    CKomplex() = default;

    /**
     * @param r Real part of the complex number
     * @param i Imaginary part
     */
    CKomplex(double r, double i) {
        this->real = r;
        this->imaginary = i;
    }


    /**
     * Init a complex number in polar form. c = e^(i*phi)
     * @param phi This number's angle in polar form
     */
    CKomplex(double phi) {
        this->real = cos(phi);
        this->imaginary = sin(phi);
    }


    /**
     * @return This number's real part
     */
    double re() {
        return this->real;
    }


    /**
     * @return This number's imaginary part
     */
    double im() {
        return this->imaginary;
    }


    /**
     * @return This number's absolute value
     */
    double abs() {
        // abs = sqrt(real^2 + im^2)
        double abs = sqrt(pow(this->real, 2) + pow(this->imaginary, 2));

        return abs;
    }

    friend CKomplex operator+ (const CKomplex & num1, const CKomplex & num2);
    friend CKomplex operator* (const CKomplex & num1, const CKomplex & num2);
    friend CKomplex operator* (const CKomplex & complex, const double & ratio);
    friend CKomplex operator* (const double & ratio, const CKomplex & complex);
    friend std::ostream& operator<< (std::ostream & out, CKomplex num);
};

//#endif //P4_CKOMPLEX_H
